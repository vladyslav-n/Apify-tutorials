## Apify Tutorials

Vladyslav Nazarenko (account: v.nazarenko@geniusee.com)

## Tutorial II - Apify SDK

*Where and how can you use JQuery with the SDK?*

> We can use it with the Puppeteer crawler after injecting it to the page with Apify.utils.

---

*What is the main difference between Cheerio and JQuery?*

> jQuery runs in a browser and attaches directly to the browser's DOM. Cherio parses html from HTTP responses, the same pages you would get in your browser when you first load a URL. The result is the typical $ function, just like in JQuery.
Cheerio doesn’t have full JQuery API

---

*When would you use CheerioCrawler and what are its limitations?*

> CheerioCrawler cannot execute JavaScript so it it is not a good fit for pages the content of which depends much on the dynamic elements. Though Cheerio is fast and needs less resources than Puppeteer, so it is a very good choice for the cases where we can avoid difficulties with dynamic content.

---

*What are the main classes for managing requests and when and why would you use one instead of another?*

> RequestList is used for making a set of URLs with which our crawling will be started. You cannot add any requests there during the process.
RequestQueue is used to add some URLs to crawl during the process.

---

*How can you extract data from a page in Puppeteer without using JQuery?*

> You may use standard methods of browser environment like document.querySelector() etc.

---

*What is the default concurrency/parallelism the SDK uses?*

> minConcurrency scale up automatically if we hadn't set this parameter before.
maxConcurrency default value equal minConcurrency
desiredConcurrency default value equal minConcurrency
desiredConcurrency default value equal minConcurrency


## Tutorial III - Apify Actors & Webhooks

*How do you allocate more CPU for your actor run?*

> It happens automatically when you increase the memory allocation for the actor's run

---

*How can you get the exact time when the actor was started from within the running actor process?*

> This info is stored in the appropriate env. variable.

---

*Which are the default storages an actor run is allocated (connected to)?*

> Every actor run is assigned with 3 default storages: KeyValue store, Dataset store and the RequestQueue store.

---

*Can you change the memory allocated to a running actor?*

> Yes — in the web app settings or in the API call props.

---

*How can you run an actor with Puppeteer in a headful (non-headless) mode?*

> Just don't use the { headless: true } and don't set the appropriate env. variable.

---

*Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)*

> It depends on how many requests the site can handle. Once you fire that out, use the least possible amount of the resources which allow the actor reach the desired concurrency. Usually cheerio needs not much resources to be performant.

---

__(Bonus - Docker)__

*What is the difference between RUN and CMD Dockerfile commands?*

> RUN lets you execute commands inside of your Docker image. These commands get executed once at build time and get written into your Docker image as a new layer.
CMD lets you define a default command to run when your container starts. This is a run-time operation.

---

*Does your Dockerfile need to contain a CMD command (assuming we don't want to use ENTRYPOINT which is similar)? If yes or no, why?*

>It doesn't have to because, for example, docker-compose can assign a CMD command for the Docker container. Also, base image may define the CMD instruction.

---

*How does the FROM command work and which base images Apify provides?*

> It lets you to choose a ready image based on which you will build your Docker app. Apify provides several images depending on the type of crawler, browser, linux version (https://github.com/apify/apify-actor-docker). The main ones are:
- apify/actor-node
- apify/actor-node-puppeteer-chrome
- apify/actor-node-playwright
- apify/actor-node-playwright-chrome
- apify/actor-node-playwright-firefox
- apify/actor-node-playwright-webkit


> Base Apify images:
>
> 1.  Node.js 12 on Alpine Linux (apify/actor-node-basic)
> 2.  Node.js 12 + Chrome on Debian (apify/actor-node-chrome)
> 3.  Node.js 12 + Chrome + Xvfb on Debian (apify actor-node-chrome-xvfb)
> 4.  [DEPRECATED] Node.js 10 + Puppeteer on Debian

## Tutorial IV - Apify CLI & Source Code

*Do you have to rebuild an actor each time the source code is changed?*

> Yes, we have to build a new image which will contain the new source code inside.
---

*What is the difference between pushing your code changes and creating a pull request?*

> When we push our changes, this changes the state in our source branch in what we are working now, or we can choose another branch. When we create a pull request we merge changes between two different branches, can see differences and other info. Also, pull request summarizing the changes between difference branches.

---

*How does the apify push command work? Is it worth using, in your opinion?*

> It pushes the source code and builds an image. It is worth using when a repository isn't used as a source for an actor.

## Tutorial V - Tasks, Storage, Apify API & Client

*What is the relationship between actor and task?*

> Actor Tasks help you prepare the configuration of an actor to perform a specific job. Tasks enable you to create multiple configurations for a single Actor and then run the selected configuration directly from the Apify platform, schedule or API.

---

*What are the differences between default (unnamed) and named storage? Which one would you choose for everyday usage?*

> Unnamed storages have limited retention time (14 days). Named storages retained indefinitely. Named and unnamed storages are the same in all regards except their retention period. The only difference is that named storages make it easier to verify we are using the correct store.
Default storages are pretty suitable for the data that will be passed to other services for the further processing.

---

*What is the relationship between the Apify API and the Apify client? Are there any significant differences?*

> Apify client implements handling the API calls with some extra functionality such as retries, waiting etc.

---

*Is it possible to use a request queue for deduplication of product IDs? If yes, how would you do that?*

> The request queue manages the process of adding the requests so that no duplicates are added.

---

*What is data retention and how does it work for all types of storage (default and named)?*

Unnamed default storages have limited retention time (14 days). Named storages are persisted indefinately.

---

*How do you pass input when running an actor or task via the API?*

> In the request body.

## Tutorial VI - Apify Proxy & Bypassing Antiscraping Software

*What types of proxies does the Apify Proxy include? What are the main differences between them?*

- The basic is one is the Datacenter proxy.
- Residential proxy - the servers are positioned geographicaly in some living areas to be more user-like.
- Google SERP proxy - for the Google search scrapers.

---

*Which proxies (proxy groups) can users access with the Apify Proxy trial? How long does this trial last?*

> 30-days basic trial gives the access to Datacenter proxies.

---

*How can you prevent a problem that one of the hardcoded proxy groups that a user is using stops working (a problem with a provider)? What should be the best practices?*

> You can prevent a blocking of proxy by using SessionPool which can filter out blocked or non-working proxies, so our actor does not retry requests over known blocked/non-working proxies.

---

*Does it make sense to rotate proxies when you are logged in?*

> The main goal of the rotation process is to imitate a user other from blocked, so the answer is no, because it will be unacceptable behaviour of basic user. User can't log in from one IP and does some actions on site from another IP.
But just in some weird cases, some sites will block the IP but not the account so you can/need to rotate even under login. That is really just the last solution.


---

*Construct a proxy URL that will select proxies only from the US (without specific groups).
What do you need to do to rotate proxies (one proxy usually has one IP)? How does this differ for Cheerio Scraper and Puppeteer Scraper?*

> http://\<session>,country-US:\<password>@proxy.apify.com:8000.
You can use a session pool for rotating the proxy IPs. You can select proxies from a specific country by passing a countryCode parameter to the proxy config.
Puppeteer rotates proxy/IP only after the browser changes (not a single page). So by default, it will use the same IP for 100 requests, unlike Cheerio.

---

*Try to set up the Apify Proxy (using any group or auto) in your browser. This is useful for testing how websites behave with proxies from specific countries (although most are from the US). You can try Switchy Omega extension but there are many more. Were you successful?*

> Yes, I used a Hola proxy and succeded.

---

*Name a few different ways a website can prevent you from scraping it.
Do you know any software companies that develop anti-scraping solutions? Have you ever encountered them on a website?*

- Limiting the request rate for a single IP address.
- Using the browser fingerprints.
- Using coockies for the user identidication
- IP blocking
> Anti-scraping software: for example, Radware Bot Manager.

## Tutorial VII - Actor Migrations & Maintaining State

*Actors have a Restart on error option in their Settings. Would you use this for your regular actors? Why? When would you use it, and when not?*

> There are many situations when this may cause a problem (especially when you are completely blocked by the site's defense so you may restart many-many times). But in some cases, when a small task needs to be done, it may really help.

---

*Migrations happen randomly, but by setting Restart on error and then throwing an error in the main process, you can force a similar situation. Observe what happens. What changes and what stays the same in a restarted actor run?*

> This option isn't a good idea, when we develop or debug our actor. Because it possibly can have some errors or bugs, which will be reproduced if this option is on, and only we can fix these errors. It can be useful, if we have tested and bug-less actor and if this actor has some errors, which don't related to our code (like connection error etc.), "Restart on error" option will restart our actor, and these errors can be fixed by this restart. This option more suitable for production version of our actor.

---

*Why don't you usually need to add any special code to handle migrations in normal crawling/scraping? Is there a component that essentially solves this problem for you?*

> Yes, special events are emitted (migration and persistState) and have been handled by the SDK itself.

---

*How can you intercept the migration event? How much time do you need after this takes place and before the actor migrates?*

> On listening to the migration and persistState events and handling them. You have several seconds during which you can do some actions.

---

*When would you persist data to a default key-value store and when would you use a named key-value store?*

> Default key-value store is good when we don't need to save our data to a long period, also default key-value store attached to a specific run of actor. We prefer to use named key-value store when we need to store data for a long period. Also, it doesn't attach to a specific run, and we can simple reach it.
