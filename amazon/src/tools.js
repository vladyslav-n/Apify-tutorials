const Apify = require('apify');
const { NAMED_KV_STORE } = require('./consts');

const { utils: { log } } = Apify;

exports.scheduleStats = async function (statsStore, interval) {
    setInterval(async () => {
        const stats = {};
        const storeKeys = [];

        await statsStore.forEachKey((key) => {
            storeKeys.push(key);
        });

        const storeValues = await Promise.all(storeKeys.map((key) => statsStore.getValue(key)));
        storeKeys.forEach((key, i) => {
            stats[key] = storeValues[i];
        });
        log.info('Dataset statistics: ', stats);
    }, interval);
};

exports.purgeStatsStore = async () => {
    const defDataset = await Apify.openDataset();

    if (!await defDataset.getInfo().itemCount) {
        const statsStore = await Apify.openKeyValueStore(NAMED_KV_STORE);

        await statsStore.drop();
    }
};
