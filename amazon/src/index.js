exports.handleOffers = require('./routes/handleOffers').handleOffers;
exports.handleProduct = require('./routes/handleProduct').handleProduct;
exports.handleSearch = require('./routes/handleSearch').handleSearch;
exports.scheduleStats = require('./tools').scheduleStats;
exports.purgeStatsStore = require('./tools').purgeStatsStore;
