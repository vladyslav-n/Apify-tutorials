const Apify = require('apify');
const { PRODUCT } = require('../consts').LABELS;
const { AMAZON_BASE_URL, AMAZON_PRODUCT_PAGE_PATH } = require('../consts');

const { utils: { log } } = Apify;

exports.handleSearch = async ({ request: { userData: { keyword }, url },
    page, crawler: { requestQueue } }) => {
    log.info(`Search string: ${url}\n`);

    // waiting for the products to load;
    try {
        await page.waitForSelector('.s-asin');
    } catch (error) {
        log.info(`No search results for the keyword '${keyword}'`);
    }

    const asins = await page.$$eval('.s-asin', (products) => {
        return products.map((el) => el.getAttribute('data-asin'));
    });

    for (const ASIN of asins) {
        await requestQueue.addRequest({
            url: AMAZON_BASE_URL + AMAZON_PRODUCT_PAGE_PATH + ASIN,
            userData: { label: PRODUCT, ASIN, keyword },
        });
    }
};
