const Apify = require('apify');
const { OFFERS } = require('../consts').LABELS;
const { AMAZON_BASE_URL, AMAZON_OFFERS_PAGE_PATH } = require('../consts');

const { utils: { log } } = Apify;

exports.handleProduct = async ({ request: { userData: { ASIN, keyword }, url },
    page, crawler: { requestQueue } }) => {
    // await page.waitForTimeout(1500);

    const titleP = page.$eval('#title', ((el) => el.textContent?.trim()));
    const descriptionP = page.$$eval('#feature-bullets li:not([data-doesntfitmessage])',
        ((items) => items.map((li) => li.textContent?.trim()).join('\n')));

    const [
        title,
        description,
    ] = await Promise.all([
        titleP,
        descriptionP,
    ]);

    const productInfo = {
        title,
        url,
        description,
        keyword,
    };

    if (await page.$('#exports_desktop_undeliverable_buybox')) {
        log.info(`${url}: undeliverable product to the server's region.`);
        return;
    }

    await requestQueue.addRequest({
        url: AMAZON_BASE_URL + AMAZON_OFFERS_PAGE_PATH + ASIN,
        userData: { label: OFFERS, ASIN, productInfo },
    }, { forefront: true });
};
