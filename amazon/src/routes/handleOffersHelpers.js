const Apify = require('apify');
const { NEEDS_MORE_SCROLL, NO_OFFERS_LEFT } = require('../consts').SCROLL_STATUS;
const { MAX_SCROLL_ITERATIONS } = require('../consts');

const { utils: { log } } = Apify;

exports.scrapePinnedOffer = async function (page, productInfo, ASIN, statsStore) {
    const sellerNameP = page.$eval('#aod-offer-soldBy [aria-label]',
        ((el) => el.textContent?.trim()));
    const priceP = page.$eval('#aod-price-0 .a-offscreen',
        ((el) => el.textContent));
    const shippingPriceP = page.evaluate(() => {
        const el = document.querySelector('#aod-bottlingDepositFee-0 + .a-size-base');
        if (!el) return 'free';
        return el.textContent?.trim().split(' ')[1].trim();
    });

    const [
        sellerName,
        price,
        shippingPrice,
    ] = await Promise.all([
        sellerNameP,
        priceP,
        shippingPriceP,
    ]);

    await Apify.pushData({
        ...productInfo,
        sellerName,
        price,
        shippingPrice,
    });

    await statsStore.setValue(ASIN, +await statsStore.getValue(ASIN) + 1);
};

exports.scrapeAdditionalOffers = async function (page, productInfo, ASIN, statsStore) {
    const offersInfos = await page.$$eval('#aod-offer',
        (offers) => {
            return offers.map((offer) => {
                const sellerName = offer.querySelector('#aod-offer-soldBy [aria-label]')
                    .textContent?.trim();
                const price = offer.querySelector('.a-offscreen').textContent;

                const shippingPriceEl = offer.querySelector('#delivery-message');
                const shippingPrice = shippingPriceEl
                    ? shippingPriceEl.textContent?.trim()
                        .split(' ')[0]
                        .toLowerCase()
                    : 'free';

                return {
                    sellerName,
                    price,
                    shippingPrice,
                };
            });
        });

    for (const offerInfo of offersInfos) {
        await Apify.pushData({
            ...productInfo,
            ...offerInfo,
        });
    }
    await statsStore.setValue(ASIN, +await statsStore.getValue(ASIN) + offersInfos.length);
};

exports.lazyLoadScroll = async function (page) {
    let offersCount;
    log.debug('Starting lazy load scroll.');

    for (let i = 0; i < MAX_SCROLL_ITERATIONS; i++) {
        offersCount = (await page.$$('#aod-offer')).length;

        // scroll down
        await page.$eval('#aod-offer:last-of-type', (scroller) => {
            scroller.scrollIntoView(true);
        });

        // return if all the offers are fetched already
        if (!await page.$('#aod-end-of-results.aod-hide')) {
            log.debug(`returning from scroll with offersCount == ${offersCount}`);
            return NO_OFFERS_LEFT;
        }
        // continue only if succeded in fetching more offers
        try {
            await page.waitForFunction(
                `document.querySelectorAll('#aod-offer').length > ${offersCount}`,
            );
        } catch (err) {
            log.error('Lazy scroll timeout.');
            return NO_OFFERS_LEFT;
        }

        // we have to do the "show more" scroll afterwards
        if (!await page.$('#aod-show-more-offers.aod-hide')) {
            return NEEDS_MORE_SCROLL;
        }
    }
};

exports.showMoreScroll = async function (page) {
    log.debug('Starting show more scroll');
    let offersCount;
    const showMoreSelector = '#aod-show-more-offers';

    for (let i = 0; i < MAX_SCROLL_ITERATIONS; i++) {
        offersCount = (await page.$$('#aod-offer')).length;

        // return if all the offers are fetched already
        if (!await page.$('#aod-end-of-results.aod-hide')) {
            return;
        }

        log.debug('Clicking the "Show more" button.');
        await page.click(showMoreSelector);

        try {
            // continue only if succeded in fetching more offers
            await page.waitForFunction(
                `document.querySelectorAll('#aod-offer').length > ${offersCount}`,
            );
        } catch (err) {
            log.error('Show more scroll timeout.');
            return;
        }
    }
};
