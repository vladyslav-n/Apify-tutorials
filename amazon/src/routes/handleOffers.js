const Apify = require('apify');
const { NEEDS_MORE_SCROLL } = require('../consts').SCROLL_STATUS;
const { NAMED_KV_STORE } = require('../consts');
const { scrapePinnedOffer, scrapeAdditionalOffers,
    lazyLoadScroll, showMoreScroll } = require('./handleOffersHelpers');

const { utils: { log } } = Apify;

exports.handleOffers = async ({ request, page, session, crawler: { requestQueue } }) => {
    const { userData: { productInfo, ASIN }, url } = request;
    const statsStore = await Apify.openKeyValueStore(NAMED_KV_STORE);

    log.debug("Waiting for the offers page's side element to load");
    try {
        await page.waitForSelector('#aod-offer-soldBy [aria-label]');
        await page.waitForSelector('#aod-price-0 .a-offscreen');
    } catch (e) {
        log.error(`url: ${url}: Timeout on the offers page.`);
        session.markBad();
        await requestQueue.reclaimRequest(request);
        return;
    }
    log.debug('Offers loaded');

    // scraping the pinned offer from the top of the page
    await scrapePinnedOffer(page, productInfo, ASIN, statsStore);

    // case when there are no additional offers
    if (await page.$('.aod-no-offer-normal-font')) {
        log.debug('There are no additional offers');
        return;
    }

    const scrollStatus = await lazyLoadScroll(page);
    if (scrollStatus === NEEDS_MORE_SCROLL) await showMoreScroll(page);

    log.debug(`url: ${url}: Fetched ${(await page.$$('#aod-offer')).length} additional offers`);

    // scrape all the additional offers
    await scrapeAdditionalOffers(page, productInfo, ASIN, statsStore);
};
