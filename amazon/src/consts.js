exports.LABELS = {
    SEARCH: 'SEARCH',
    PRODUCT: 'PRODUCT',
    OFFERS: 'OFFERS',
};

exports.SCROLL_STATUS = {
    NEEDS_MORE_SCROLL: 'NEEDS_MORE_SCROLL',
    NO_OFFERS_LEFT: 'NO_OFFERS_LEFT',
};

exports.MAX_SCROLL_ITERATIONS = 100;

exports.NAMED_KV_STORE = 'amazon-stats-store';

exports.DEFAULT_PROXY_GROUP = 'BUYPROXIES94952';

exports.AMAZON_BASE_URL = 'https://www.amazon.com';
exports.AMAZON_PRODUCT_PAGE_PATH = '/dp/';
exports.AMAZON_OFFERS_PAGE_PATH = '/gp/offer-listing/';
