const Apify = require('apify');
const { handleSearch, handleProduct, handleOffers,
    scheduleStats, purgeStatsStore } = require('./src/index');
const { SEARCH, OFFERS, PRODUCT } = require('./src/consts').LABELS;
const { NAMED_KV_STORE, DEFAULT_PROXY_GROUP } = require('./src/consts');

const { utils: { log } } = Apify;

Apify.main(async () => {
    const { keyword, maxConcurrency, headless = false, proxyCountryCode,
        maxRequestsPerCrawl, statsInterval = 20000 } = await Apify.getInput();
    const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`;
    const requestList = await Apify.openRequestList('start-url', [
        { url: startUrl, userData: { label: SEARCH, keyword } },
    ]);
    const requestQueue = await Apify.openRequestQueue();
    const proxyConfiguration = await Apify.createProxyConfiguration({
        countryCode: proxyCountryCode,
        groups: [DEFAULT_PROXY_GROUP],
    });

    await purgeStatsStore();
    const statsStore = await Apify.openKeyValueStore(NAMED_KV_STORE);
    scheduleStats(statsStore, statsInterval);

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        proxyConfiguration,
        useSessionPool: true,
        persistCookiesPerSession: true,
        sessionPoolOptions: {
            maxPoolSize: 100,
            sessionOptions: {
                maxUsageCount: 5,
            },
        },
        maxRequestsPerCrawl,
        maxConcurrency,
        launchContext: {
            // Chrome with stealth should work for most websites.
            useChrome: false,
            stealth: true,
            launchOptions: {
                headless,
            },
        },
        handlePageFunction: async (context) => {
            const { page, request, session, crawler: { requestQueue: reqQueue } } = context;
            const { url, userData: { label } } = request;
            log.info('Page opened.', { label, url });
            switch (label) {
                case PRODUCT:
                    if (!await page.$('#title')) {
                        log.warning(`url ${url}: request failed, sending back to the queue`);
                        session.retire();
                        await reqQueue.reclaimRequest(request);
                        return;
                    }
                    return handleProduct(context);
                case OFFERS:
                    return handleOffers(context);
                case SEARCH:
                    return handleSearch(context);

                default:
                    log.error(`Unrecognized label '${label}'.`);
            }
        },
        handleFailedRequestFunction: async ({ request }) => {
            log.error(`Request ${request.url} failed too many times`);
            await Apify.pushData({
                '#debug': Apify.utils.createRequestDebugInfo(request),
            });
        },

    });

    log.info('Starting the crawl.');
    await crawler.run();
    log.info('Crawl finished.');

    // send email
    await Apify.call('apify/send-mail', {
        to: 'lukas@apify.com',
        subject: 'Vladyslav Nazarenko. This is for the Apify Tutorials',
        text: `The link to the dataset:
    https://api.apify.com/v2/datasets/${process.env.APIFY_DEFAULT_DATASET_ID}/items`,
    });
});
