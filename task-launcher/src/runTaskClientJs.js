const Apify = require('apify');
const ApifyClient = require('apify-client');

const client = new ApifyClient({
    token: process.env.APIFY_TOKEN,
});
const { utils: { log } } = Apify;

exports.runTaskClientJs = async function ({ taskId, memory, maxItems, fields }) {
    const taskClient = client.task(taskId);

    /**
     * Since taskClient.call() can wait for the task to end indefinitely
     * we don't need a polling loop as in case of raw API calls which max
     * wait time is 300 secs.
     */
    const run = await taskClient.call({}, { memory });
    log.debug('run: ', run);

    const { defaultDatasetId, id } = run;

    if (!id) {
        log.error('Invalid task parameters');
        await Apify.setValue('OUTPUT', {
            result: 'Invalid task parameters',
        });
        return;
    }

    const datasetClient = client.dataset(defaultDatasetId);
    const options = {
        clean: true,
        limit: maxItems,
        fields,
        skipHidden: true,
    };

    const data = await datasetClient.downloadItems('csv', options);
    await saveData(data);
};

async function saveData(data) {
    if (!data) {
        log.error('Failed to fetch the dataset');

        await Apify.setValue('OUTPUT', {
            result: 'Failed to fetch the dataset',
        });
        return;
    }
    await Apify.setValue('OUTPUT', data, { contentType: 'text/csv' });
}
