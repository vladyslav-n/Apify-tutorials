const Apify = require('apify');
const axios = require('axios');
const { SUCCEEDED, ABORTED, FAILED } = require('./consts').RUN_STATUS;

const { utils: { log } } = Apify;

exports.runTaskAPI = async function ({ taskId, memory, maxItems, fields }) {
    const baseUrl = `https://api.apify.com/v2/actor-tasks/${taskId}/runs`;
    const params = {
        memory,
        token: process.env.APIFY_TOKEN,
    };

    const taskRunResponse = await axios.post(baseUrl, {}, { params });
    const { data: { data: { defaultDatasetId, id } } } = taskRunResponse;
    log.debug('taskRunResponse: ', taskRunResponse.data.data);

    if (!id) {
        log.error('Invalid task parameters');
        await Apify.setValue('OUTPUT', {
            result: 'Invalid task parameters',
        });
        return;
    }

    await refresh({ maxItems, fields, defaultDatasetId, runId: id });
};

async function refresh({ maxItems, fields, defaultDatasetId, runId }) {
    await Apify.utils.sleep(5000);

    const runUrl = `https://api.apify.com/v2/actor-runs/${runId}`;
    const runStatusResponse = await axios.get(runUrl);
    const { data: { data: { status } } } = runStatusResponse;

    log.debug('taskRunResponse: ', runStatusResponse.data.data);
    log.info(`status of the actor run: ${status}`);

    if (status === SUCCEEDED) {
        log.info('Actor succedeed. Saving it results to the OUTPUT.csv');
        await saveDataAPI(defaultDatasetId, maxItems, fields);
    } else if (status && status !== FAILED && status !== ABORTED) {
        await refresh({ maxItems, fields, defaultDatasetId, runId });
    } else {
        log.error('Actor did not reach state "SUCCEEDED"');
        await Apify.setValue('OUTPUT', {
            result: 'Actor did not reach state "SUCCEEDED"',
        });
    }
}

async function saveDataAPI(datasetId, maxItems, fields) {
    const runUrl = `https://api.apify.com/v2/datasets/${datasetId}/items`;
    const params = {
        format: 'csv',
        clean: true,
        limit: maxItems,
        fields: fields.join(','),
        skipHidden: true,
    };

    const { data } = await axios.get(runUrl, { params });

    if (!data) {
        log.error('Failed to fetch the dataset');

        await Apify.setValue('OUTPUT', {
            result: 'Failed to fetch the dataset',
        });
        return;
    }
    await Apify.setValue('OUTPUT', data, { contentType: 'text/csv' });
}
