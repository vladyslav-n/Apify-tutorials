const Apify = require('apify');
const { runTaskAPI, runTaskClientJs } = require('./src/index');

const { utils: { log } } = Apify;

Apify.main(async () => {
    log.info('Starting the actor');

    const input = await Apify.getInput();
    if (!input) throw new Error('Have you passed the correct INPUT ?');
    log.info('INPUT: ', input);

    if (input.useClient === false) {
        await runTaskAPI(input);
    } else {
        await runTaskClientJs(input);
    }
});
